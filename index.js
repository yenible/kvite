/*
 * @Descripttion: 
 * @version: 
 * @Author: yenible
 * @Date: 2022-10-08 20:39:12
 */

// 学习视频地址：
// https://www.bilibili.com/video/BV1dh411S7Vz/?p=7&spm_id_from=pageDriver&vd_source=18ee0d2c832887993a955746838a2044
const Koa = require('koa')

const app = new Koa()

const fs = require('fs')
const path = require('path')
const compilerSFC = require('@vue/compiler-sfc')
const compilerDom = require('@vue/compiler-dom')

app.use(async ctx => {
    const { url, query } = ctx.request
    if (url === '/') {
        // 加载index
        ctx.body = fs.readFileSync(path.join(__dirname, './index.html'), 'utf8')
    } else if (url.endsWith('.js')) {
        // js结尾的字符串
        // 获取当前服务器的绝对di'z地址
        const p = path.join(__dirname, url)
        ctx.type = 'application/javascript'
        // 把读出来的内容进行裸模块全局替换
        ctx.body = rewriteImport(fs.readFileSync(p, 'utf8'))
    } else if (url.startsWith('/@modules/')) {
        // 获取模块名称
        const moduleName = url.replace('/@modules/', '')
        // 去node_modules目录里面找

        const prefix = path.join(__dirname, './node_modules', moduleName)

        //package.json中获取module字段
        const module = require(prefix + '/package.json').module

        const filePath = path.join(prefix, module)
        const ret = fs.readFileSync(filePath, 'utf8')
        ctx.type = 'application/javascript'
        // 把读出来的内容进行裸模块全局替换
        ctx.body = rewriteImport((ret))
    } else if (url.indexOf('.vue') > -1) {
        // 目标：读取vue文件解析为js
        const p = path.join(__dirname, url.split('?')[0])
        const ret = compilerSFC.parse(fs.readFileSync(p, 'utf8'))
        if (!query.type) {
            // sfc请求

            // 获取脚本的内容
            const scriptContent = ret.descriptor.script.content
            // 替换默认导出为常量，方便后续的修改
            const script = scriptContent.replace('export default ', 'const __script = ')
            console.log(`
            ${rewriteImport(script)}
            // 解析tpl
            import {render as __render} from '${url}?type=template'
            __script.render = __render
            export default __script
            `)
            ctx.type = 'application/javascript'
            ctx.body = `
        ${rewriteImport(script)}
        // 解析tpl
        import {render as __render} from '${url}?type=template'
        __script.render = __render
        export default __script
        `
        }else if(query.type === 'template'){
            // 有查询参数,解析tpl
            const tpl =  ret.descriptor.script.content
            const render = compilerDom.compile(tpl, {mode: 'module'}).code
            console.log('render================')
            console.log(render)
            ctx.type = 'application/javascript'
            ctx.body = rewriteImport(render)
        }

    }

})

// 裸模块地址重写
// import xx from 'xxx'
// import xx from '@modules/xxx'
function rewriteImport(content) {
    return content.replace(/from\s+["'](.*)['"]/g, function (s1, s2) {
        if (s2.startsWith('./') || s2.startsWith('/') || s2.startsWith('../')) {
            return s1
        } else {
            // 裸模块，需要替换
            return ` from '/@modules/${s2}'`
        }
    })
}
app.listen(3000, () => {
    console.log('kvite server')
})  