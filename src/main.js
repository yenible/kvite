/*
 * @Descripttion: 
 * @version: 
 * @Author: yenible
 * @Date: 2022-10-08 20:51:16
 */
import {createApp, h}     from    'vue'
import app from './app.vue'
createApp(app).mount('#app')